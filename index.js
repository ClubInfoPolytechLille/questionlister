const express = require('express')
const fs = require('fs');
const bodyParser = require("body-parser");
const app = express();
const port = 3000;

const header = "<h1>Bienvenido les SE5</h1>" +
    "<form action=\".\" method='post'>" +
    "<input type=\"text\" id=\"fname\" name=\"fname\"><br>" +
    "<input type=\"submit\" value=\"Submit\">" +
    "</form><br>";

app.use(bodyParser.urlencoded({ extended: false }));

app.post('/', (req, res) => {
    params = new URLSearchParams((req.body.fname))

    //console.log(params)

    let question = params.get("valeur[9]")
    let answer = params.get("valeur[15]")
    let answerShort = params.get("valeur[16]")

    fs.appendFile("dataList.txt", "\n" + question + '$(' + answerShort + ')' + answer, "utf8", () => {})

    res.send('Merci pour la nouvelle réponse !')
})

app.get('/', (req, res) => {
    fs.readFile("dataList.txt", "utf8", (err,data) => {
        console.log(data)
        res.send(header + data.replaceAll("\n", "</br>"))
    });

})

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})